# Macrosoft QT

*Macrosoft QT* is a group of four M.Sc. students in Software Engineering working on integrating large language models in the QT robot.

## Members
- @eric-gilles
- @Muddinana
- @pablolaviron
- @siob10

## Repositories

You will find two repositories:
- [GPT Speech Interaction](https://gitlab.com/macrosoft-qt/gpt_speech_interaction): the source code of our final catkin package (written in Python / ROS)
- [Research](https://gitlab.com/macrosoft-qt/research): Jupyter notebooks about our research on integrating LLMs in robotics, including emotion analysis and more
